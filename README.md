### Links
[The Discord Server](https://discord.gg/h84SvhsARQ)

[My YouTube Channel](https://youtube.com/@oglothenerd)



### About This Game
The game is inspired by other voxel games like Minecraft and CardLife (CardLife is actually a voxel game with marching cubes). I started this game because I wasn't happy with the "modern" survival game feel. I also just like voxels in general! I wanted a simple sandbox experience where you team up, build a base, and just have fun! Kind of like the early days of Minecraft! This is my dream game. I am working very hard on it, so I hope you enjoy it! This project is FOSS under the GPL 3.0 license. So, feel free to contribute code! That always helps!

-- Jackson Novak (The Creator)