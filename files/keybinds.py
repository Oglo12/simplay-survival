class Keybinds:
    binds = {
            "walk_forward": "w",
            "walk_backward": "s",
            "walk_left": "a",
            "walk_right": "d",
            "jump": "space",
            "sprint": "left shift",
            "sneak": "left control",
            }
