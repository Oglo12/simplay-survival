class World:
    limits = {
            "+x": None,
            "-x": None,
            "+y": None,
            "-y": -10,
            "+z": None,
            "-z": None,
            }

    axis_directions = ["+x", "-x", "+y", "-y", "+z", "-z"]
