from ursina import *

class Unit(Button):
    def __init__(self, position = (0, 0, 0), model = None, collision = True, hit_box = True, size = (1, 1, 1)):
        super().__init__(
                collision = collision,
                hit_box = hit_box,
                size = size,
                model = model,
                texture = None,
                scale = None,
                position = (0, 0, 0),
                parent = scene,
                spawn_ran = False,
                )

    def update(self):
        if self.spawn_ran == False:
            self.when_spawned()
            self.spawn_ran = True

        self.loop()

    def when_spawned(self):
        pass

    def loop(self):
        pass

class Player(Unit):
    def __init__(self, position = ()):
        super().__init__(
                model = 'cube',
                size = (1, 2, 1),
                position = position,
                )

    def input(self, key):
        if held_keys["w"] == True:
            self.position += self.forward
