from perlin_noise import PerlinNoise
from random import randint
from numpy import floor

class Perlin:
    seed = randint(0, 999999999999999)

    terrain_config = {"amp": 10, "freq": 52, "octaves": 8}

    def terrain(x, z, seed = seed, terrain_config = terrain_config):
        terrain_noise = PerlinNoise(seed = seed, octaves = terrain_config.get("octaves"))

        y = floor(terrain_noise([x / terrain_config.get("freq"), z / terrain_config.get("freq")]) * terrain_config.get("amp"))

        return y
