from ursina import load_texture

class Content:
    textures = {
            "debug": "assets/textures/block/debug.png",
            "dirt": "assets/textures/block/dirt.png",
            "grass_top": "assets/textures/block/grass_top.png",
            "grass_side": "assets/textures/block/grass_side.png",
            "stone": "assets/textures/block/stone.png",
            }
    blocktype_flag_defaults = {
            "solid": False,
            "transparent": False,
            "custom_model": False,
            "persistent": False,
            }
    blocktypes = {
            "air": {
                "solid": False,
                "transparent": True,
                "custom_model": False,
                "+x": None,
                "-x": None,
                "+y": None,
                "-y": None,
                "+z": None,
                "-z": None,
                },
            "debug": {
                "solid": True,
                "transparent": False,
                "custom_model": False,
                "+x": textures.get("debug"),
                "-x": textures.get("debug"),
                "+y": textures.get("debug"),
                "-y": textures.get("debug"),
                "+z": textures.get("debug"),
                "-z": textures.get("debug"),
                },
            "dirt": {
                "solid": True,
                "transparent": False,
                "custom_model": False,
                "+x": textures.get("dirt"),
                "-x": textures.get("dirt"),
                "+y": textures.get("dirt"),
                "-y": textures.get("dirt"),
                "+z": textures.get("dirt"),
                "-z": textures.get("dirt"),
                },
            "grass": {
                "solid": True,
                "transparent": False,
                "custom_model": False,
                "+x": textures.get("grass_side"),
                "-x": textures.get("grass_side"),
                "+y": textures.get("grass_top"),
                "-y": textures.get("dirt"),
                "+z": textures.get("grass_side"),
                "-z": textures.get("grass_side"),
                },
            "stone": {
                "solid": True,
                "transparent": False,
                "custom_model": False,
                "+x": textures.get("stone"),
                "-x": textures.get("stone"),
                "+y": textures.get("stone"),
                "-y": textures.get("stone"),
                "+z": textures.get("stone"),
                "-z": textures.get("stone"),
                },
            }
