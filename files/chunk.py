from ursina import Entity
from numpy import floor, ceil

from perlin import Perlin
import voxel
from game_data import GameData

class ChunkConfig:
    size_x = 4
    size_y = 2
    size_z = 4
    size = (size_x, size_y, size_z)

class ChunkData:
    objects = {}

class Chunk(Entity):
    def __init__(self, chunk_pos = (0, 0, 0)):
        super().__init__(
                chunk_pos = chunk_pos,
                position = (chunk_pos[0] * ChunkConfig.size_x, chunk_pos[1] * ChunkConfig.size_y, chunk_pos[2] * ChunkConfig.size_z),
                model = None,
                texture = None,
                color = None,
                scale = 1,
                spawn_ran = False,
                )

    def update(self):
        if self.spawn_ran == False:
            self.when_spawned()
            self.spawn_ran = True

        self.loop()

    def loop(self):
        pass

    def when_spawned(self):
        ChunkData.objects[self.chunk_pos] = self

        self.gen_terrain()

    def gen_terrain(self):
        for xz in range(ChunkConfig.size_x * ChunkConfig.size_z):
            x = floor(xz / ChunkConfig.size_x) + self.position[0]
            z = floor(xz % ChunkConfig.size_z) + self.position[2]

            for y in range(int(ChunkConfig.size_y + self.position[1])):
                block = self.get_or_new_blocktype(x, y, z)

                voxel.set_block(x, y, z, block)

    def new_blocktype(self, x, y, z):
        p = Perlin.terrain(x, z)
        
        if y > p:
            return "air"

        if y == p:
            return "grass"

        elif y < p:
            return "dirt"

        else:
            return "air"

    def get_or_new_blocktype(self, x, y, z):
        if self.get_block(x, y, z) == None:
            return self.new_blocktype(x, y, z)
        else:
            return self.get_block(x, y, z)

    def get_block(self, x, y, z):
        return GameData.blocks_chunked.get((self.chunk_pos, (x, y, z)))

class ChunkHandler(Entity):
    def __init__(self, following = None):
        super().__init__(
                following = following,
                spawn_ran = False,
                )

    def update(self):
        if self.spawn_ran == False:
            self.when_spawned()
            self.spawn_ran = True

        self.position = following.position
        self.current_chunk = snap_to_chunk(following.x, following.y, following.z)

        # Chunk code will go here!

    def when_spawned(self):
        pass

def snap_to_chunk(x, y, z):
    return_value = [None, None, None]
    pos = (x / ChunkConfig.size_x, y / ChunkConfig.size_y, z / ChunkConfig.size_z)

    for i in range(3):
        abs_num = abs(pos[i])
        ceil_num = ceil(abs_num)

        if pos[i] >= 0:
            return_value[i] = ceil_num
        else:
            return_value[i] = ceil_num * -1

    return (return_value[0], return_value[1], return_value[2])

def pos_to_chunk_pos(x, y, z):
    return (x % ChunkConfig.size_x, y % ChunkConfig.size_y, z % ChunkConfig.size_z)
