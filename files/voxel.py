from ursina import *

from content import Content
from game_data import GameData
from world import World
from chunk import *

class BlockData:
    objects = {}

class Blocks:
    def refresh_all():
        for i in BlockData.objects:
            i.refresh()

class Block(Entity):
    def __init__(self, position = (0, 0, 0), blocktype = "debug", data = {}, flags = {}):
        super().__init__(
                model = None,
                texture = None,
                color = color.rgb(255, 255, 255),
                position = (position[0] / 2, position[1] / 2, position[2] / 2),
                input_pos = None,
                blocktype = Content.blocktypes.get(blocktype),
                blocktype_id = blocktype,
                faces = {},
                face_directions = ["+x", "-x", "+y", "-y", "+z", "-z"],
                data = data,
                flags = flags,
                chunk = None,
                spawn_ran = False,
                )

    def update(self):
        if self.spawn_ran == False:
            self.input_pos = (self.position[0] * 2, self.position[1] * 2, self.position[2] * 2)

            self.chunk = snap_to_chunk(self.input_pos[0], self.input_pos[1], self.input_pos[2])

            GameData.blocks[self.input_pos] = self.blocktype
            GameData.blocks_id[self.input_pos] = self.blocktype_id
            GameData.blocks_chunked = [(self.chunk, pos_to_chunk_pos(self.input_pos[0], self.input_pos[1], self.input_pos[2]))]
            BlockData.objects[self.input_pos] = self
            self.init_faces()

            self.spawn_ran = True

        for i in self.face_directions:
            if self.is_face(i) != self.should_be_face(i):
                if self.is_face(i) == True:
                    self.delete_face(i)
                else:
                    self.init_face(i)

        if self.get_block_flag("persistent") == False and len(self.faces) == 0:
            self.delete_self()
            return

        self.loop()

    def loop(self):
        pass

    def refresh(self):
        for i in self.face_directions:
            destroy(self.faces.get(i))

        self.faces = {}

        self.init_faces()

    def is_face(self, way):
        if self.faces.get(way) == None:
            return False
        else:
            return True

    def should_be_face(self, way):
        checks = {
                "+x": (self.input_pos[0] + 1, self.input_pos[1] + 0, self.input_pos[2] + 0),
                "-x": (self.input_pos[0] + -1, self.input_pos[1] + 0, self.input_pos[2] + 0),
                "+y": (self.input_pos[0] + 0, self.input_pos[1] + 1, self.input_pos[2] + 0),
                "-y": (self.input_pos[0] + 0, self.input_pos[1] + -1, self.input_pos[2] + 0),
                "+z": (self.input_pos[0] + 0, self.input_pos[1] + 0, self.input_pos[2] + 1),
                "-z": (self.input_pos[0] + 0, self.input_pos[1] + 0, self.input_pos[2] + -1),
                }

        if self.blocktype.get(way) == None:
            return False

        if GameData.blocks.get(checks.get(way)) == None:
            return True
        else:
            if GameData.blocks.get(checks.get(way)).get("transparent") == True:
                return True
            else:
                return False

    def delete_face(self, way):
        if self.faces.get(way) != None:
            destroy(self.faces.get(way))
            self.faces[way] = None

    def init_face(self, way):
        if self.should_be_face(way) == True:
            self.faces[way] = BlockFace(way, self.blocktype, self.position, self)

    def init_faces(self):
        for i in World.axis_directions:
            self.init_face(i)

    def delete_self(self):
        BlockData.objects[self.input_pos] = None
        destroy(self)

    def get_block_flag(self, flag):
        if self.flags.get(flag) != None:
            return self.flags.get(flag)
        elif self.blocktype.get(flag) != None:
            return self.blocktype.get(flag)
        else:
            return Content.blocktype_flag_defaults.get(flag)

class BlockFace(Button):
    def __init__(self, way, blocktype, position, parent):
        super().__init__(
                way = way,
                position = position,
                input_pos = None,
                parent = parent,
                blocktype = blocktype,
                rotation = (0, 0, 0),
                spawn_ran = False,
                model = "quad",
                texture = None,
                color = color.rgb(255, 255, 255),
                origin = (0, 0, 0.5),
                double_sided = False,
                )

    def update(self):
        if self.spawn_ran == False:
            if self.way == "+x":
                self.rotation = (0, -90, 0)
            elif self.way == "-x":
                self.rotation = (0, 90, 0)
            elif self.way == "+y":
                self.rotation = (90, 0, 0)
            elif self.way == "-y":
                self.rotation = (-90, 0, 0)
            elif self.way == "+z":
                self.rotation = (0, 180, 0)
            elif self.way == "-z":
                self.rotation = (0, 0, 0)

            self.texture = self.blocktype.get(self.way)
            self.color = self.get_lighting()

            spawn_ran = True

        self.input_pos = (self.position[0] * 2, self.position[1] * 2, self.position[2] * 2)

    def get_lighting(self):
        if self.way == "+y":
            return color.rgb(255, 255, 255)
        elif self.way == "-y":
            return color.rgb(30, 30, 30)
        elif self.way == "+x":
            return color.rgb(200, 200, 200)
        elif self.way == "-x":
            return color.rgb(100, 100, 100)
        elif self.way == "+z":
            return color.rgb(150, 150, 150)
        elif self.way == "-z":
            return color.rgb(100, 100, 100)
        else:
            return color.rgb(0, 100, 100)

def get_block(x, y, z, allow_none = False):
    block = GameData.blocks_id.get((x, y, z))

    if block == None:
        if allow_none == False:
            return "air"
        else:
            return block
    else:
        return block

def delete_block(x, y, z):
    if BlockData.objects.get((x, y, z)) != None:
        destroy(BlockData.objects.get((x, y, z)))
        BlockData.objects[(x, y, z)] = None
        GameData.blocks[(x, y, z)] = None
        GameData.blocks_id[(x, y, z)] = None
        GameData.blocks_chunked[(BlockData.objects.get((x, y, z)).chunk, (x, y, z))] = None

def set_block(x, y, z, block, data = {}):
    if GameData.blocks.get((x, y, z)) != None:
        delete_block(x, y, z)

    Block(position = (x, y, z), blocktype = block, data = data)

def reset_block(x, y, z):
    delete_block(x, y, z)
    GameData.blocks[(x, y, z)] = self.chunk.new_blocktype(x, y, z)
    set_block(x, y, z, get_block(x, y, z))
