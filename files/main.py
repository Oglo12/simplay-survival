"""
---- TODO ----
1. Add comments to the code.
2. Add an entity class and make a custom player controller.
3. Keybind support.
4. Make game go brrrrrrrr!
--------------
"""

from ursina import *
from numpy import floor
import time as ti

from voxel import *
from game_data import GameData
from perlin import Perlin
from chunk import ChunkHandler
from unit import *

window.borderless = False

app = Ursina()

player = Player()

chunker = ChunkHandler(following = player)

def update():
    pass

app.run()
